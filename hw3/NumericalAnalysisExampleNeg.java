//this test case checks if you consider all print statements and find atleast one that has a negative print.
class NumericalAnalysisExampleNeg{
    public static void main(String[] a){
        System.out.println(new A().id());
    }
}

class A {
    public int id(){
        int i;
	i  = 5;
        System.out.println(i-6);
        return 1;
    }
}


